-----------------------------
-- Mix method test
-----------------------------

require "field"

-- calculate control sum to ensure that elements are shuffled and not regenerated
local function sum_grid(grid)
	local sum = 0

	grid:iterate(
		function(x, y)
			local value = grid:get(x, y)

			if type(value) == "number" then
				sum = sum + value
			end
		end
	)

	return sum
end

math.randomseed(os.time())

local config = {
	x = 6,
	y = 6,
	element_types = {0, 1, 2, 3, 4, 5, 6}
}

local field = Field:new(config)

-- generate field with no moves
field.grid:fill(
	function(x, y)
		return {
			type = config.element_types[math.fmod((y - 1) * 10 + x - 1, #config.element_types) + 1],
			marked_for_removal = false
		}
	end
)

while true do
	local result, grid, points = field:tick() -- tick
	dump(
		{
			grid = grid.data,
			stats = {
				["Points"] = points,
				["[Debug] Sum"] = sum_grid(grid)
			}
		}
	)

	if result == UPDATE_RESULT.CONTINUE then
		sleep(500)
	elseif result == UPDATE_RESULT.STOP then
		local move = sequences.find_move(field.grid, field.sequence_length, Field.element_ops)
		local result = field:move(move.from, move.to) -- move

		if result == MOVE_RESULT.INCORRECT_COORDS then
			print("Incorrect coordinates given")
			return
		elseif result == MOVE_RESULT.INCORRECT_DIRECTION then
			print("Incorrect direction given")
			return
		end
	elseif result == UPDATE_RESULT.NO_MOVES then
		print("No moves left, mix...")
		sleep(2000)
		field:mix() -- mix
	end
end
