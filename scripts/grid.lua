-----------------------------
-- Grid container
-----------------------------

Grid = {}

function iterate_size(size, func)
	for x = 1, size.x do
		for y = 1, size.y do
			func(x, y)
		end
	end
end

function Grid:new(size)
	o = {
		data = {},
		size = size
	}

	for x = 1, size.x do
		o.data[x] = {}
	end

	setmetatable(o, self)
	self.__index = self

	return o
end

function Grid:get(x, y)
	return self.data[x][y]
end

function Grid:column(x)
	return self.data[x]
end

function Grid:width()
	return self.size.x
end

function Grid:height()
	return self.size.y
end

function Grid:iterate(func)
	return iterate_size(self.size, func)
end

function Grid:copy()
	local grid = Grid:new(self.size)

	grid:fill(
		function(x, y)
			return self:get(x, y)
		end
	)

	return grid
end

function Grid:transform(func)
	local grid = Grid:new(self.size)

	grid:fill(
		function(x, y)
			return func(self:get(x, y))
		end
	)

	return grid
end

function Grid:fill(generator)
	self:iterate(
		function(x, y)
			self:set(x, y, generator(x, y))
		end
	)
end

function Grid:set(x, y, value)
	self.data[x][y] = value
end

function Grid:swap(from, to)
	self.data[to.x][to.y], self.data[from.x][from.y] = self.data[from.x][from.y], self.data[to.x][to.y]
end
