-----------------------------
-- Automatic game run
-----------------------------

-----------------------------
-- config

local field_config = {}

function FieldConfig(config)
	if type(config.x) ~= "number" or type(config.y) ~= "number" or config.x < 1 or config.x < 1 or config.x > 10 or config.y > 10 then
		error("Incorrect field dimentions!")
	end

	field_config = config
end

require "config"

-----------------------------

require "field"

math.randomseed(os.time())

local SPEED = 2

local field = Field:new(field_config)

field:init() 																-- init

while true do
	local result, grid, points = field:tick() 								-- tick
	dump({grid = grid.data, stats = {["Points"] = points}})

	if result == UPDATE_RESULT.CONTINUE then
		sleep(500 / SPEED)
	elseif result == UPDATE_RESULT.STOP then
		local move = sequences.find_move(field.grid, field.sequence_length, Field.element_ops)
		local result = field:move(move.from, move.to) 						-- move

		if result == MOVE_RESULT.INCORRECT_COORDS then
			print("Incorrect coordinates given")
			return
		elseif result == MOVE_RESULT.INCORRECT_DIRECTION then
			print("Incorrect direction given")
			return
		end
	elseif result == UPDATE_RESULT.NO_MOVES then
		print("No moves left, mix...")
		sleep(2000)
		field:mix() 														-- mix
	end
end
