-----------------------------
-- Input processing facility
-----------------------------

require("transforms")

input = {}

INPUT_RESULT = {}

INPUT_RESULT.EMPTY_COMMAND,
INPUT_RESULT.UNKNOWN_COMMAND,
INPUT_RESULT.INCORRECT_USAGE,
INPUT_RESULT.QUIT,
INPUT_RESULT.MOVE = 0, 1, 2, 3, 4

local function quit_command(args)
	return INPUT_RESULT.QUIT
end

local function move_command(args)
	local _, _, x, y, direction = string.find(args, "(%d)(%d)(%a)")

	if x == nil then
		return INPUT_RESULT.INCORRECT_USAGE, {usage = "m{x}{y}{l|r|u|d}"}
	end

	local from = {x = tonumber(x) + 1, y = tonumber(y) + 1}
	local to = transforms.transform_point(direction, from)

	return INPUT_RESULT.MOVE, {from = from, to = to}
end

local commands = {
	q = quit_command,
	m = move_command
}

function input.process(code_sequence)
	if #code_sequence == 0 then
		return INPUT_RESULT.EMPTY_COMMAND
	end

	local pos, _, key, args = string.find(code_sequence, "(%a)(.*)")
	local command = commands[key]

	if command == nil then
		return INPUT_RESULT.UNKNOWN_COMMAND, {command = key}
	end

	return command(args)
end
