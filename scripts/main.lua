-----------------------------
-- config

local field_config = {}

function FieldConfig(config)
	if type(config.x) ~= "number" or type(config.y) ~= "number" or config.x < 1 or config.x < 1 or config.x > 10 or config.y > 10 then
		error("Incorrect field dimentions!")
	end

	field_config = config
end

require "config"

-----------------------------

require "field"
require "input"

local function show_message(message)
	print(message .. ". Press any key")
	get_key()
end

-----------------------------
-- start

math.randomseed(os.time())

local field = Field:new(field_config)

field:init() 														-- init

local function handle_input()
	local result, args = input.process(get_line())

	if result == INPUT_RESULT.MOVE then
		local result = field:move(args.from, args.to) 				-- move

		if result == MOVE_RESULT.INCORRECT_COORDS then
			show_message("Incorrect coordinates given")
		elseif result == MOVE_RESULT.INCORRECT_DIRECTION then
			show_message("Incorrect direction given")
		end
	elseif result == INPUT_RESULT.QUIT then
		os.exit(0)
	elseif result == INPUT_RESULT.UNKNOWN_COMMAND then
		show_message("Unknown command: " .. args.command .. ". Use m{x}{y}{d} to move or q to quit")
	elseif result == INPUT_RESULT.INCORRECT_USAGE then
		show_message("Incorrect arguments. Usage: " .. args.usage)
	elseif result == INPUT_RESULT.EMPTY_COMMAND then
		-- ignore
	end
end

while true do
	local result, grid, points = field:tick() 						-- tick
	dump({grid = grid.data, stats = {["Points"] = points}})

	if result == UPDATE_RESULT.CONTINUE then
		sleep(500)
	elseif result == UPDATE_RESULT.STOP then
		handle_input()
	elseif result == UPDATE_RESULT.NO_MOVES then
		show_message("No moves left, mix needed")
		field:mix() 												-- mix
	end
end
