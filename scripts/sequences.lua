-----------------------------
-- Sequence processing facilities
-----------------------------

require "grid"
require "transforms"

sequences = {}

local function correct_ranges(xrange, yrange, size)
	return {
		math.max(1, math.min(size.x, math.min(xrange[1], xrange[2]))),
		math.max(1, math.min(size.x, math.max(xrange[1], xrange[2])))
	}, {
		math.max(1, math.min(size.y, math.min(yrange[1], yrange[2]))),
		math.max(1, math.min(size.y, math.max(yrange[1], yrange[2])))
	}
end

-- search for the first sequence
function sequences.find(grid, xrange, yrange, sequence_length, ops)
	local xrange, yrange = correct_ranges(xrange, yrange, grid.size)
	local previous = nil
	local counter = 0

	local function reset()
		counter = 0
	end

	local function collect(x, y)
		local value = grid:get(x, y)

		if previous and not ops.equal(previous, value) then
			reset()
		end

		previous = value
		counter = counter + 1
	end

	-- columns
	for x = xrange[1], xrange[2] do
		for y = 1, grid:height() do
			collect(x, y)

			if counter >= sequence_length then
				return true
			end
		end

		reset()
	end

	-- rows
	for y = yrange[1], yrange[2] do
		for x = 1, grid:width() do
			collect(x, y)

			if counter >= sequence_length then
				return true
			end
		end

		reset()
	end

	return false
end

-- search for sequences and mark them
function sequences.mark(grid, sequence_length, ops)
	local sequences = {}
	local previous = nil
	local current_sequence = {}

	local function reset()
		if #current_sequence >= sequence_length then
			table.insert(sequences, current_sequence)
		end

		current_sequence = {}
	end

	local function collect(x, y)
		local value = grid:get(x, y)

		if previous and not ops.equal(previous, value) then
			reset()
		end

		previous = value
		table.insert(current_sequence, {x = x, y = y})
	end

	-- columns
	for x = 1, grid:width() do
		for y = 1, grid:height() do
			collect(x, y)
		end

		reset()
	end

	-- rows
	for y = 1, grid:height() do
		for x = 1, grid:width() do
			collect(x, y)
		end

		reset()
	end

	for _, sequence in ipairs(sequences) do
		for _, point in ipairs(sequence) do
			ops.mark(grid:get(point.x, point.y))
		end
	end

	return #sequences > 0
end

-- find the first acceptable move which creates sequence
function sequences.find_move(grid, sequence_length, ops)
	local grid = grid:copy()

	for y = 1, grid:height() do
		for x = 1, grid:width() - 1 do
			local from = {x = x, y = y}
			local to = transforms.transform_point("r", from)
			grid:swap(from, to)

			if sequences.find(grid, {from.x, to.x}, {from.y, to.y}, sequence_length, ops) then
				return {from = from, to = to}
			end

			grid:swap(to, from)
		end
	end

	for x = 1, grid:width() do
		for y = 1, grid:height() - 1 do
			local from = {x = x, y = y}
			local to = transforms.transform_point("d", from)
			grid:swap(from, to)

			if sequences.find(grid, {from.x, to.x}, {from.y, to.y}, sequence_length, ops) then
				return {from = from, to = to}
			end

			grid:swap(to, from)
		end
	end

	return nil -- no moves
end
