-----------------------------
-- Field model
-----------------------------

require "sequences"
require "debug"

-----------------------------
-- results

UPDATE_RESULT = {}
UPDATE_RESULT.CONTINUE,
UPDATE_RESULT.STOP,
UPDATE_RESULT.NO_MOVES = 0, 1, 2

MOVE_RESULT = {}
MOVE_RESULT.OK,
MOVE_RESULT.INCORRECT_COORDS,
MOVE_RESULT.INCORRECT_DIRECTION = 0, 1, 2

-----------------------------
-- private methods forward declaration

local private = {}

-----------------------------
-- Field

Field = {}

-- states

local STATE = {}
STATE.SEARCH,               -- search for sequences and available moves
STATE.USER_MOVE,            -- apply user move
STATE.CHECK_VALID_MOVE,     -- search for available sequences after user move
STATE.FALLING = 0, 1, 2, 3  -- remove marked elements and fall

local state_handlers = {}

-- create model instance from configuration
function Field:new(config)
    -- select type generator
	local generator = config.generator or function(x, y)
		return config.element_types[math.random(#config.element_types)]
	end

	o = {
		grid = Grid:new({x = config.x, y = config.y}),
		sequence_length = 3,
		state = STATE.SEARCH,
		points = 0,
		generator = function(x, y) return {type = generator(x, y), marked_for_removal = false} end,
		last_move = nil
	}

	setmetatable(o, self)
	self.__index = self

	return o
end

-- initialize field with random elements
function Field:init()
	repeat
		self.grid:fill(self.generator)

        -- remove sequences
		while sequences.mark(self.grid, self.sequence_length, Field.element_ops) do
			while private.fall(self.grid, self.generator) do
			end
		end
	until sequences.find_move(self.grid, self.sequence_length, Field.element_ops)
end

-- perform state update
function Field:tick()
    -- handle current state
	local result, state = state_handlers[self.state](self)
	self.state = state or STATE.SEARCH

	return result, self.grid:transform(Field.element_ops.to_string), self.points
end

-- perform move
function Field:move(from, to)
	if from == nil or from.x < 1 or from.x < 1 or from.x > self.grid:width() or from.y > self.grid:height() then
		return MOVE_RESULT.INCORRECT_COORDS
	end

	if to == nil or to.x < 1 or to.x < 1 or to.x > self.grid:width() or to.y > self.grid:height() then
		return MOVE_RESULT.INCORRECT_DIRECTION
	end

    -- save user move for further update
	self.last_move = {from = from, to = to}
	self.state = STATE.USER_MOVE

	return MOVE_RESULT.OK
end

-- shuffle elements to generate new available moves
function Field:mix()
	repeat
        -- randomize grid
		self.grid:iterate(
			function(x, y)
				local from = {x = x, y = y}

				while true do
					local to = {x = math.random(self.grid:width()), y = math.random(self.grid:height())}
					self.grid:swap(from, to)

                    -- avoid sequences
					if not sequences.find(self.grid, {from.x, to.x}, {from.y, to.y}, self.sequence_length, Field.element_ops) then
						break
					end

					self.grid:swap(to, from)
				end
			end
		)
	until sequences.find_move(self.grid, self.sequence_length, Field.element_ops)
end

-----------------------------
-- handlers

state_handlers[STATE.SEARCH] = function(self)
	if private.process_sequences(self) then
		return UPDATE_RESULT.CONTINUE, STATE.FALLING
	end

	if not sequences.find_move(self.grid, self.sequence_length, Field.element_ops) then
		return UPDATE_RESULT.NO_MOVES
	end

	return UPDATE_RESULT.STOP
end

state_handlers[STATE.USER_MOVE] = function(self)
	local from = self.last_move.from
	local to = self.last_move.to

	self.grid:swap(from, to)                            -- apply user move

	return UPDATE_RESULT.CONTINUE, STATE.CHECK_VALID_MOVE
end

state_handlers[STATE.CHECK_VALID_MOVE] = function(self)
	if private.process_sequences(self) then
		return UPDATE_RESULT.CONTINUE, STATE.FALLING    -- move was valid, start falling
	end

	local from = self.last_move.from
	local to = self.last_move.to

	self.grid:swap(to, from)                            -- revert move

	return UPDATE_RESULT.STOP
end

state_handlers[STATE.FALLING] = function(self)
	if private.fall(self.grid, self.generator) then
		return UPDATE_RESULT.CONTINUE, STATE.FALLING    -- continue fall
	end

	if private.process_sequences(self) then
		return UPDATE_RESULT.CONTINUE, STATE.FALLING    -- fall again
	end

	return UPDATE_RESULT.CONTINUE, STATE.SEARCH
end

-----------------------------
-- element operations, extracted to remove coupling with the actual element structure

Field.element_ops = {
	equal = function(a, b)
		return a.type == b.type
	end,
	is_marked = function(element)
		return element.marked_for_removal
	end,
	mark = function(element)
		element.marked_for_removal = true
	end,
	to_string = function(element)
		if element.marked_for_removal then
			return " "
		end

		return element.type
	end
}
-----------------------------
-- private

-- find sequences on grid, mark them and charge points for marked elements
function private.process_sequences(self)
	-- find and mark completed sequences for removal
	if not sequences.mark(self.grid, self.sequence_length, Field.element_ops) then
		return false
	end

	-- customization point for special elements:
	-- private.handle_special_elements(self.grid, function() end)

	self.points = self.points + private.calculate_points(self.grid)
	return true
end

function private.handle_special_elements(grid, handler)
	grid:iterate(
		function(x, y)
			local element = grid:get(x, y)

			if element.marked_for_removal then
				handler(grid, x, y, element.type)
			end
		end
	)
end

-- count elements marked for removal
function private.calculate_points(grid)
	local points = 0

	grid:iterate(
		function(x, y)
			if grid:get(x, y).marked_for_removal then
				points = points + 1
			end
		end
	)

	return points
end

-- fall by one line
function private.fall(grid, generator)
	local has_change = false

	for x = 1, grid:width() do
		for y = grid:height(), 1, -1 do
			local column = grid:column(x)

			if column[y].marked_for_removal then
				table.remove(column, y)
				table.insert(column, 1, generator(x, y))
				has_change = true
				break -- go to the next column
			end
		end
	end

	return has_change
end
