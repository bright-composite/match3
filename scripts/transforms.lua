transforms = {
	["l"] = {x = -1, y = 0},
	["u"] = {x = 0, y = -1},
	["r"] = {x = 1, y = 0},
	["d"] = {x = 0, y = 1}
}

function transforms.transform_point(direction, point)
	local transform = transforms[direction]

	if not transform then
		return nil
	end

	return {x = point.x + transform.x, y = point.y + transform.y}
end
