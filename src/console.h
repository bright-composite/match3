#ifndef CONSOLE_H
#define CONSOLE_H

void init_console();

void push_buffer();
void pop_buffer(int height);

void clear_screen();
void toggle_cursor(bool show);
char read_char();

#endif // CONSOLE_H