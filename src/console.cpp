#include "console.h"

#ifdef WIN32

#include <Windows.h>
#include <conio.h>
#include <vector>

static HANDLE stdout_handle;
static HANDLE screen_buffer_handle;
static COORD console_buffer_size{80, 32};
static std::vector<CHAR_INFO> console_buffer;

static const COORD BUFFER_COORDS{0, 0};
static CONSOLE_SCREEN_BUFFER_INFO csbi;

void init_console()
{
    stdout_handle = GetStdHandle(STD_OUTPUT_HANDLE);
    // create screen buffer to avoid console flickering
    screen_buffer_handle = CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CONSOLE_TEXTMODE_BUFFER, NULL);
}

void push_buffer()
{
    GetConsoleScreenBufferInfo(stdout_handle, &csbi);
    console_buffer_size.X = csbi.dwSize.X;
    console_buffer_size.Y = csbi.dwSize.Y;

    console_buffer.resize(console_buffer_size.X * console_buffer_size.Y);

    SetConsoleScreenBufferSize(screen_buffer_handle, console_buffer_size);
    SetConsoleActiveScreenBuffer(screen_buffer_handle);
}

void pop_buffer(int height)
{
    SMALL_RECT rect{0, 0, console_buffer_size.X - 1, SHORT(height)};

    ReadConsoleOutput(stdout_handle, console_buffer.data(), console_buffer_size, BUFFER_COORDS, &rect);
    WriteConsoleOutput(screen_buffer_handle, console_buffer.data(), console_buffer_size, BUFFER_COORDS, &rect);

    SetConsoleActiveScreenBuffer(stdout_handle);
}

void clear_screen()
{
    DWORD console_size = csbi.dwSize.X * csbi.dwSize.Y;
    DWORD chars_written = 0;

    if (!FillConsoleOutputCharacter(stdout_handle, (TCHAR)' ', console_size, BUFFER_COORDS, &chars_written))
    {
        return;
    }

    if (!FillConsoleOutputAttribute(stdout_handle, csbi.wAttributes, console_size, BUFFER_COORDS, &chars_written))
    {
        return;
    }

    SetConsoleCursorPosition(stdout_handle, BUFFER_COORDS);
}

void toggle_cursor(bool show)
{
    CONSOLE_CURSOR_INFO info;
    info.dwSize = 100;
    info.bVisible = show ? TRUE : FALSE;
    SetConsoleCursorInfo(stdout_handle, &info);
}

char read_char()
{
    return static_cast<char>(_getch());
}

#else

#include <unistd.h>
#include <termios.h>
#include <iostream>

void init_console() {}

void push_buffer() {}
void pop_buffer(int height) {}

void clear_screen()
{
    std::cout << "\x1B[2J\x1B[H";
}

void toggle_cursor(bool)
{
    // not implemented
}

char read_char()
{
    char buf = 0;
    struct termios old = {0};

    if (tcgetattr(0, &old) < 0) {
        return 0;
    }

    old.c_lflag &= ~ICANON;
    old.c_lflag &= ~ECHO;
    old.c_cc[VMIN] = 1;
    old.c_cc[VTIME] = 0;

    if (tcsetattr(0, TCSANOW, &old) < 0) {
        return 0;
    }

    if (read(0, &buf, 1) < 0) {
        return 0;
    }

    old.c_lflag |= ICANON;
    old.c_lflag |= ECHO;

    if (tcsetattr(0, TCSADRAIN, &old) < 0) {
        return 0;
    }

    return buf;
}

#endif
