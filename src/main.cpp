#include <string>
#include <map>
#include <iostream>
#include <sstream>
#include <thread>
#include <chrono>

#include "console.h"

extern "C"
{
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}

/**
 * @brief dump
 *      Read grid data from lua state stack and print it on the screen
 * @param  L:
 * @return - always 0
 */
static int dump(lua_State * L)
{
    if (!lua_istable(L, 1)) {
        std::cerr << "Incorrect parameter";
        return 0;
    }

    // read points
    lua_getfield(L, 1, "stats");
	std::map<std::string, int> stats;

	lua_pushnil(L);

	while (lua_next(L, -2) != 0) {
		std::string key = luaL_checkstring(L, -2);
    	stats[key] = static_cast<int>(luaL_checknumber(L, -1));
   		lua_pop(L, 1);
	}

    lua_pop(L, 1);

    // read grid
    lua_getfield(L, 1, "grid");

    if (!lua_istable(L, -1)) {
        std::cerr << "Incorrect grid type" << std::endl;
        return 0;
    }

    size_t sizex = lua_rawlen(L, -1);

    // read first column
    lua_rawgeti(L, -1, 1);

    if (!lua_istable(L, -1)) {
        std::cerr << "Incorrect column type" << std::endl;
        return 0;
    }

    size_t sizey = lua_rawlen(L, -1);
    lua_pop(L, 1);

    // start dumping

    std::string buffer;
    std::stringstream out;

    out << "    ";

    for (int x = 1; x <= sizex; ++x) {
        out << (x - 1) << " ";
    }

    out << "     ";

	for (auto & entry : stats) {
    	out << entry.first << ": " << entry.second << "   ";
	}

    out << std::endl;

    out << "  +-";

    for (int x = 1; x <= sizex; ++x) {
        out << "--";
    }

    out << "+" << std::endl;

    for (int y = 1; y <= sizey; ++y) {
        out << (y - 1) << " | ";

        for (int x = 1; x <= sizex; ++x) {
            lua_rawgeti(L, -1, x);
            lua_rawgeti(L, -1, y);

            out << luaL_checkstring(L, -1) << " ";

            lua_pop(L, 1);
            lua_pop(L, 1);
        }

        out << "| " << (y - 1) << std::endl;
    }

    lua_pop(L, 1);

    out << "  +-";

    for (int x = 1; x <= sizex; ++x) {
        out << "--";
    }

    out << "+" << std::endl;

    out << "    ";

    for (int x = 1; x <= sizex; ++x) {
        out << (x - 1) << " ";
    }

    out << std::endl << std::endl;

    std::string s = out.str();

    push_buffer();

    clear_screen();
    std::cout.write(s.data(), s.size());
    std::cout.flush();

    pop_buffer(static_cast<int>(sizey * 2 + 7));

    return 0;
}

/**
 * @brief get_line
 *      Read input line and push it into lua state stack
 * @param  L:
 * @return - always 1
 */
static int get_line(lua_State * L)
{
    toggle_cursor(true);

    std::cout << "> ";

    std::string input;
    std::getline(std::cin, input);

    toggle_cursor(false);

    lua_pushstring(L, input.c_str());

    return 1;
}

/**
 * @brief get_key
 *      Read one character
 * @param  L:
 * @return - always 1
 */
static int get_key(lua_State * L)
{
    char ch = read_char();
    lua_pushstring(L, &ch);

    return 1;
}

static int sleep(lua_State * L)
{
    int64_t msec = static_cast<int64_t>(luaL_checknumber(L, 1));

    std::this_thread::sleep_for(std::chrono::milliseconds(msec));

    return 0;
}

static void load_script(lua_State * L, const std::string & filename)
{
    if (luaL_loadfile(L, filename.c_str()) != 0) {
        std::cerr << "Couldn't load file: " << filename << std::endl;
        return;
    }

    if (lua_pcall(L, 0, LUA_MULTRET, 0) != 0) {
        std::cerr << "Lua error: " << lua_tostring(L, -1) << std::endl;
        return;
    }

    clear_screen();
}

int main(int argc, char * argv[])
{
    init_console();

    clear_screen();

    lua_State * L = luaL_newstate();
    luaL_openlibs(L);

    lua_pushcfunction(L, dump);
    lua_setglobal(L, "dump");
    lua_pushcfunction(L, get_line);
    lua_setglobal(L, "get_line");
    lua_pushcfunction(L, get_key);
    lua_setglobal(L, "get_key");
    lua_pushcfunction(L, sleep);
    lua_setglobal(L, "sleep");

    std::string script = argc > 1 ? argv[1] : "main";

    toggle_cursor(false);

#ifdef DEBUG
    luaL_dostring(L, "package.path = package.path .. ';../../scripts/?.lua'");
    luaL_dostring(L, "package.cpath = package.cpath .. ';../../scripts/?.lua'");

    load_script(L, "../../scripts/" + script + ".lua");
#else
    luaL_dostring(L, "package.path = package.path .. ';../scripts/?.lua'");
    luaL_dostring(L, "package.cpath = package.cpath .. ';../scripts/?.lua'");

    load_script(L, "../scripts/" + script + ".lua");
#endif

    toggle_cursor(true);

    lua_close(L);

    return 0;
}
