# Match-3
Console prototype of the "match-3" game written on Lua

## Usage
`./match3`                - launch game (main.lua)  
`./match3 auto`           - launch automatic game run  
`./match3 <script_name>`  - launch <script_name>.lua  

Enter **m{x-coord}{y-coord}{direction}** to move element at {x-coord},{y-coord} to {direction}. Available directions are l (left), r (right), u (up) and d (down). Example: **m53d**  
Enter **q** to quit
